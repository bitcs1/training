const express = require('express')
const fs = require('fs')
const mongoose = require('mongoose')
const Cat = require('./models/cat')

const app = express()

const dbURI = 'mongodb+srv://test:test1234@cluster0.r2e2e.mongodb.net/Cats?retryWrites=true&w=majority'
mongoose.connect(dbURI, {useNewUrlParser: true, useUnifiedTopology: true})
    .then( () => app.listen(8080))
    .catch( (err) => console.log(err))

app.get('/', (req,res) => {
  res.sendFile('./views/html.home', {root: __dirname})
})

app.post('/add', (req,res) => {
  const cat = new Cat(req.body);

  cat.save()
    .then((result) => res.send(result))
    .catch((err) => console.log(err))
  };

  app.get('/search', (req,res) => {
    Cat.findById(req.body)
    .then((result) => res.send(result))
    .catch((err) => console.log(err))
  })
})
